package com.pulsar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/15 14:03:03<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@SpringBootApplication
public class PulsarApplication {

    public static void main(String[] args) {
        SpringApplication.run(PulsarApplication.class, args);
    }
}
