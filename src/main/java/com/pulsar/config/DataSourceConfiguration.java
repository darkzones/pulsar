package com.pulsar.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.config<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/15/2018 14:52 Sunday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Configuration
@MapperScan("com.pulsar.dao") // 扫描所有Mapper类，避免在每个Mapper类上面加@Mapper注解
public class DataSourceConfiguration {

    @Value("${spring.datasource.driver-class-name}")
    private String jdbcDriver;
    @Value("${spring.datasource.url}")
    private String jdbcUrl;
    @Value("${spring.datasource.username}")
    private String userName;
    @Value("${spring.datasource.password}")
    private String passWord;
    @Value("${spring.datasource.maxActive}")
    private int maxActive;
    @Value("${spring.datasource.initialSize}")
    private int initialSize;
    @Value("${spring.datasource.maxWait}")
    private long maxWait;
    @Value("${spring.datasource.minIdle}")
    private int minIdle;

    @Bean(name = "dataSource")
    public DruidDataSource createDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(jdbcDriver);
        druidDataSource.setUrl(jdbcUrl);
        druidDataSource.setUsername(userName);
        druidDataSource.setPassword(passWord);
//        druidDataSource.setMaxActive(maxActive);
//        druidDataSource.setInitialSize(initialSize);
//        druidDataSource.setMaxWait(maxWait);
//        druidDataSource.setMinIdle(minIdle);
        druidDataSource.setDefaultAutoCommit(false);
        return druidDataSource;
    }
}
