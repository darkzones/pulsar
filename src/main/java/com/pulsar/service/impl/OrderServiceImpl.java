package com.pulsar.service.impl;

import com.pulsar.dao.OrderDao;
import com.pulsar.entity.Order;
import com.pulsar.entity.OrderRel;
import com.pulsar.entity.User;
import com.pulsar.entity.vo.GoodsVo;
import com.pulsar.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service.impl<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/8 16:27 星期三<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderDao orderDao;

    @Override
    public OrderRel getOrderRelByUserIdAndGoodsId(Long userId, Long goodsId) {
        return orderDao.getOrderRelByUserIdAndGoodsId(userId, goodsId);
    }

    @Override
    @Transactional
    public Order createOrder(User user, GoodsVo goodsVo) {
        Order order = new Order();
        order.setOrderCreateTime(new Date());
        order.setAddrId(0L);
        order.setGoodsCount(1);
        order.setGoodsId(goodsVo.getGoodsId());
        order.setGoodsName(goodsVo.getGoodsName());
        order.setGoodsPrice(goodsVo.getSeckillPrice());
        order.setOrderChannel(1);
        order.setOrderStatus(0);
        order.setUserId(user.getUserId());
        Long orderId = orderDao.insertOrder(order);
        OrderRel orderRel = new OrderRel();
        orderRel.setGoodsId(goodsVo.getGoodsId());
        orderRel.setOrderId(orderId);
        orderRel.setUserId(user.getUserId());
        orderDao.insertOrderRel(orderRel);
        return order;
    }
}
