package com.pulsar.service.impl;

import com.pulsar.entity.GoodsSeckill;
import com.pulsar.entity.Order;
import com.pulsar.entity.User;
import com.pulsar.entity.vo.GoodsVo;
import com.pulsar.service.GoodsService;
import com.pulsar.service.OrderService;
import com.pulsar.service.SeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service.impl<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/8 16:36 星期三<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Service
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    GoodsService goodsService;

    @Autowired
    OrderService orderService;

    @Override
    @Transactional
    public Order seckill(User user, GoodsVo goodsVo) {
        GoodsSeckill goodsSeckill = new GoodsSeckill();
        goodsSeckill.setGoodsId(goodsVo.getGoodsId());
        goodsSeckill.setSeckillStockCount(goodsVo.getSeckillStockCount());
        goodsService.reduceStock(goodsSeckill);
        return orderService.createOrder(user, goodsVo);
    }
}
