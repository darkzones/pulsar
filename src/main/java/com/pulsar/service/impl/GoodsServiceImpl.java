package com.pulsar.service.impl;

import com.pulsar.dao.GoodsDao;
import com.pulsar.entity.GoodsSeckill;
import com.pulsar.entity.vo.GoodsVo;
import com.pulsar.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service.impl<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/6 21:36 星期一<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<GoodsVo> getGoodsVoList() {
        return goodsDao.getGoodsVoList();
    }

    @Override
    public GoodsVo getGoodsVoById(Long goodsId) {
        return goodsDao.getGoodsVoById(goodsId);
    }

    @Override
    public void reduceStock(GoodsSeckill goodsSeckill) {
        goodsDao.reduceStock(goodsSeckill);
    }
}
