package com.pulsar.service;

import com.pulsar.entity.Order;
import com.pulsar.entity.User;
import com.pulsar.entity.vo.GoodsVo;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/8 16:24 星期三<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface SeckillService {

    Order seckill(User user, GoodsVo goodsVo);
}
