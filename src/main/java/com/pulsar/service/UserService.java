package com.pulsar.service;

import com.pulsar.entity.User;
import com.pulsar.entity.vo.LoginVo;

import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/30/2018 19:48 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface UserService {

    User getUserById(long userId);

    User getByToken(HttpServletResponse response, String token);

    boolean login(HttpServletResponse response, LoginVo loginVo);

}
