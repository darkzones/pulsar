package com.pulsar.service;

import com.pulsar.entity.Area;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.service<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/15/2018 16:46 Sunday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface AreaService {

    List<Area> getAreaList();

    Area getAreaById(Integer areaId);

    boolean addArea(Area area);

    boolean updArea(Area area);

    boolean delArea(Integer areaId);
}
