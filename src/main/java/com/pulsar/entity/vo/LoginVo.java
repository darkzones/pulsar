package com.pulsar.entity.vo;

import javax.validation.constraints.NotNull;

import com.pulsar.validator.IsMobile;
import org.hibernate.validator.constraints.Length;

/**
 * 功能描述: 登录VO<br>
 * 所属包名: com.pulsar.entity.vo<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 19:08:50<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class LoginVo {

    @NotNull
    @IsMobile
    private String mobile;

    @NotNull
    @Length(min = 32)
    private String password;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginVo [mobile=" + mobile + ", password=" + password + "]";
    }
}
