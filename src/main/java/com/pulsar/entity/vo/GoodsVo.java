package com.pulsar.entity.vo;

import com.pulsar.entity.Goods;

import java.util.Date;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.entity.vo<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/6 21:08 星期一<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class GoodsVo extends Goods {

    private Double seckillPrice;
    private Integer seckillStockCount;
    private Date seckillStartTime;
    private Date seckillEndTime;

    public Double getSeckillPrice() {
        return seckillPrice;
    }

    public void setSeckillPrice(Double seckillPrice) {
        this.seckillPrice = seckillPrice;
    }

    public Integer getSeckillStockCount() {
        return seckillStockCount;
    }

    public void setSeckillStockCount(Integer seckillStockCount) {
        this.seckillStockCount = seckillStockCount;
    }

    public Date getSeckillStartTime() {
        return seckillStartTime;
    }

    public void setSeckillStartTime(Date seckillStartTime) {
        this.seckillStartTime = seckillStartTime;
    }

    public Date getSeckillEndTime() {
        return seckillEndTime;
    }

    public void setSeckillEndTime(Date seckillEndTime) {
        this.seckillEndTime = seckillEndTime;
    }
}
