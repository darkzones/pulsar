package com.pulsar.dao;

import com.pulsar.entity.Order;
import com.pulsar.entity.OrderRel;
import org.apache.ibatis.annotations.Param;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.dao<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/8 16:29 星期三<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface OrderDao {

    OrderRel getOrderRelByUserIdAndGoodsId(@Param("userId") Long userId, @Param("goodsId") Long goodsId);

    Long insertOrder(Order order);

    void insertOrderRel(OrderRel orderRel);
}
