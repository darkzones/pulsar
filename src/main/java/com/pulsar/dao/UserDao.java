package com.pulsar.dao;

import com.pulsar.entity.User;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.dao<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/30/2018 19:33 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface UserDao {

    User getUserById(long userId);
}
