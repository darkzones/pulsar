package com.pulsar.dao;

import com.pulsar.entity.GoodsSeckill;
import com.pulsar.entity.vo.GoodsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.dao<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/8/6 21:11 星期一<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface GoodsDao {

    List<GoodsVo> getGoodsVoList();

    GoodsVo getGoodsVoById(@Param("goodsId") Long goodsId);

    void reduceStock(GoodsSeckill goodsSeckill);
}
