package com.pulsar.dao;

import com.pulsar.entity.Area;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.dao<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/15/2018 15:23 Sunday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface AreaDao {

    List<Area> queryArea();

    Area queryAreaById(int areaId);

    int insertArea(Area area);

    int updateArea(Area area);

    int deleteArea(int areaId);
}
