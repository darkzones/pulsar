package com.pulsar.validator;

import com.pulsar.util.ValidatorUtil;
import org.apache.commons.lang.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 功能描述: 验证手机号格式<br>
 * 所属包名: com.pulsar.validator<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 19:07:30<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class IsMobileValidator implements ConstraintValidator<IsMobile, String> {

    private boolean required = false;

    public void initialize(IsMobile constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (required) {
            return ValidatorUtil.isMobile(value);
        }

        if (StringUtils.isEmpty(value)) {
            return true;
        } else {
            return ValidatorUtil.isMobile(value);
        }
    }

}
