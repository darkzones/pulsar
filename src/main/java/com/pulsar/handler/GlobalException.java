package com.pulsar.handler;

import com.pulsar.message.ResponseCode;

/**
 * 功能描述: 自定义全局异常<br>
 * 所属包名: com.pulsar.handler<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 19:11:56<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class GlobalException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private ResponseCode rc;

    public GlobalException(ResponseCode rc) {
        super(rc.toString());
        this.rc = rc;
    }

    public ResponseCode getRc() {
        return rc;
    }

}
