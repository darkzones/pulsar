package com.pulsar.message;

/**
 * 功能描述: 通用返回结果<br>
 * 所属包名: com.pulsar.message<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 13:46:42<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class Result<T> {

    private int code;
    private String msg;
    private T data;

    private Result(T data) {
        this.code = 0; // 成功响应码
        this.msg = "invoke success";
        this.data = data;
    }

    private Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Result(ResponseCode ResponseCode) {
        if (ResponseCode != null) {
            this.code = ResponseCode.getCode();
            this.msg = ResponseCode.getMsg();
        }
    }

    /**
     * 成功时候的调用
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    /**
     * 失败时候的调用
     */
    public static <T> Result<T> error(ResponseCode ResponseCode) {
        return new Result<>(ResponseCode);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
