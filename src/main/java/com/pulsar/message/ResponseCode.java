package com.pulsar.message;

/**
 * 功能描述: 响应码<br>
 * 所属包名: com.pulsar.message<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/30/2018 13:44 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class ResponseCode {

    private int code;
    private String msg;

    // 通用的错误码
    public static ResponseCode SUCCESS = new ResponseCode(0, "invoke success");
    public static ResponseCode SERVER_ERROR = new ResponseCode(500100, "服务端异常");
    public static ResponseCode BIND_ERROR = new ResponseCode(500101, "参数校验异常：%s");

    // 登录模块 5002XX
    public static ResponseCode SESSION_ERROR = new ResponseCode(500210, "Session不存在或者已经失效");
    public static ResponseCode PASSWORD_EMPTY = new ResponseCode(500211, "登录密码不能为空");
    public static ResponseCode MOBILE_EMPTY = new ResponseCode(500212, "手机号不能为空");
    public static ResponseCode MOBILE_ERROR = new ResponseCode(500213, "手机号格式错误");
    public static ResponseCode MOBILE_NOT_EXIST = new ResponseCode(500214, "手机号不存在");
    public static ResponseCode PASSWORD_ERROR = new ResponseCode(500215, "密码错误");

    // 商品模块 5003XX

    // 订单模块 5004XX

    // 秒杀模块 5005XX
    public static ResponseCode SECKILL_OVER = new ResponseCode(500500, "商品已秒杀结束");
    public static ResponseCode SECKILL_REPEATE = new ResponseCode(500501, "不能重复秒杀");

    // 地区模块 5006XX
    public static ResponseCode AREA_NAME_EMPTY = new ResponseCode(500601, "地区名为空");
    public static ResponseCode AREA_ID_EMPTY = new ResponseCode(500602, "地区标识为空");
    public static ResponseCode AREA_INSERT_ERROR = new ResponseCode(500603, "插入地区信息失败");
    public static ResponseCode AREA_UPDATE_ERROR = new ResponseCode(500604, "更新地区信息失败");
    public static ResponseCode AREA_DELETE_ERROR = new ResponseCode(500605, "删除地区信息失败");

    private ResponseCode() {
    }

    private ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseCode fillArgs(Object... args) {
        int code = this.code;
        String message = String.format(this.msg, args);
        return new ResponseCode(code, message);
    }

    @Override
    public String toString() {
        return "ResponseCode [code=" + code + ", msg=" + msg + "]";
    }
}
