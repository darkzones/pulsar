package com.pulsar.redis.key;

/**
 * 功能描述: redis key前缀，避免不同模块之间的key冲突<br>
 * 所属包名: com.pulsar.redis.perfix<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 13:25:06<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface PrefixKey {

    int expireSeconds();

    String getPrefix();

}
