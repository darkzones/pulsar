package com.pulsar.redis.key;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.redis.keys<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/30/2018 13:19 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class TestKey extends BaseKey {

    private TestKey(String prefix) {
        super(prefix);
    }

    public static TestKey getById = new TestKey("id");
}
