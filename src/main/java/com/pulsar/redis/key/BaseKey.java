package com.pulsar.redis.key;

/**
 * 功能描述: 模板模式，封装key前缀基本实现<br>
 * 所属包名: com.pulsar.redis<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 12:57:21<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public abstract class BaseKey implements PrefixKey {

    private int expireSeconds;

    private String prefix;

    public BaseKey(String prefix) {
        this(0, prefix); // 0代表永不过期
    }

    public BaseKey(int expireSeconds, String prefix) {
        this.expireSeconds = expireSeconds;
        this.prefix = prefix;
    }

    public int expireSeconds() {//默认0代表永不过期
        return expireSeconds;
    }

    public String getPrefix() {
        String className = getClass().getSimpleName();
        return className + ":" + prefix;
    }

}
