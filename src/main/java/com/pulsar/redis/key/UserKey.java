package com.pulsar.redis.key;

import com.pulsar.util.ConsUtil;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.redis.keys<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 13:15:36<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class UserKey extends BaseKey {

    private UserKey(String prefix) {
        super(prefix);
    }

    private UserKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }

    public static UserKey getById = new UserKey("id");
    public static UserKey getByName = new UserKey("name");
    public static UserKey token = new UserKey(ConsUtil.TOKEN_EXPIRE_TIME, "tk");
}
