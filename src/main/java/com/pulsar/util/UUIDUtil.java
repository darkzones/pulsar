package com.pulsar.util;

import java.util.UUID;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.util<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 18:40:52<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class UUIDUtil {

	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
