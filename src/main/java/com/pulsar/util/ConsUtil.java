package com.pulsar.util;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.util<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/31/2018 22:20 Tuesday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface ConsUtil {

    String COOKI_NAME_OF_TOKEN = "token";

    int TOKEN_EXPIRE_TIME = 3600 * 24 * 2;
}
