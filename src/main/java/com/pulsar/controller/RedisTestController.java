package com.pulsar.controller;

import com.pulsar.message.ResponseCode;
import com.pulsar.message.Result;
import com.pulsar.redis.RedisClient;
import com.pulsar.redis.key.TestKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述: redis测试类<br>
 * 所属包名: com.pulsar.web<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/30/2018 13:18 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
@RequestMapping("/redis")
public class RedisTestController {

    @Autowired
    private RedisClient redisClient;

    @RequestMapping("/set")
    public Result<Boolean> redisPut() {
        boolean result = redisClient.set(TestKey.getById, "key1", "value1");
        return Result.success(result);
    }

    @RequestMapping("/get")
    public Result<String> redisGet() {
        String result = redisClient.get(TestKey.getById, "key1", String.class);
        return Result.success(result);
    }

    @RequestMapping("/error")
    public Result<String> redisError() {
        return Result.error(ResponseCode.SERVER_ERROR);
    }

    @RequestMapping("/success")
    public Result<String> redisSuccess() {
        return Result.error(ResponseCode.SUCCESS);
    }

}
