package com.pulsar.controller;

import com.pulsar.entity.vo.LoginVo;
import com.pulsar.message.Result;
import com.pulsar.redis.RedisClient;
import com.pulsar.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * 功能描述: 登录<br>
 * 所属包名: com.pulsar.controller<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/07/30 19:37:39<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Controller
public class LoginController {

    private static Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    UserService userService;

    @Autowired
    RedisClient redisClient;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/login/do")
    @ResponseBody
    public Result<Boolean> doLogin(HttpServletResponse response, @Valid LoginVo loginVo) {
        log.info(loginVo.toString());
        //登录
        userService.login(response, loginVo);
        return Result.success(true);
    }
}
