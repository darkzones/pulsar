package com.pulsar.controller;

import com.pulsar.entity.Area;
import com.pulsar.message.Result;
import com.pulsar.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.web<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/15/2018 17:04 Sunday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RestController
@RequestMapping("/area")
public class AreaController {

    @Autowired
    private AreaService areaService;

    @GetMapping("/list")
    public Result<List<Area>> getAreaList() {
        List<Area> areaList = areaService.getAreaList();
        return Result.success(areaList);
    }

    @GetMapping("/id")
    public Result<Area> getAreaById(@RequestParam(value = "areaId", required = false) Integer areaId) {
        Area area = areaService.getAreaById(areaId);
        return Result.success(area);
    }

    @PostMapping("/add")
    public Result<Boolean> addArea(@RequestBody Area area) {
        boolean result = areaService.addArea(area);
        return Result.success(result);
    }

    @PostMapping("/upd")
    public Result<Boolean> updArea(@RequestBody Area area) {
        boolean result = areaService.updArea(area);
        return Result.success(result);
    }

    @GetMapping("/del")
    public Result<Boolean> delArea(@RequestParam(value = "areaId", required = false) Integer areaId) {
        boolean result = areaService.delArea(areaId);
        return Result.success(result);
    }
}
