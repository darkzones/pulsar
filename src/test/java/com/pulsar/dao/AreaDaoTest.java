package com.pulsar.dao;

import com.pulsar.entity.Area;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * 功能描述: <br>
 * 所属包名: com.pulsar.dao<br>
 * 创建人　: 白剑<br>
 * 创建时间: 07/15/2018 16:00 Sunday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AreaDaoTest {

    @Autowired
    private AreaDao areaDao;

    @Test
    @Ignore
    public void queryArea() {
        List<Area> areaList = areaDao.queryArea();
        assertEquals(2, areaList.size());
    }

    @Test
    public void queryAreaById() {
        Area area = areaDao.queryAreaById(2);
        assertEquals("华北", area.getAreaName());
    }

    @Test
    @Ignore
    public void insertArea() {
        Area area = new Area();
        area.setAreaName("华西");
        area.setPriority(3);
        area.setCreateTime(new Date());
        area.setLastEditTime(new Date());
        int rowNo = areaDao.insertArea(area);
        assertEquals(1, rowNo);
    }

    @Test
    @Ignore
    public void updateArea() {
        Area area = new Area();
        area.setAreaId(3);
        area.setAreaName("华南");
        int rowNo = areaDao.updateArea(area);
        assertEquals(1, rowNo);
    }

    @Test
    @Ignore
    public void deleteArea() {
        int rowNo = areaDao.deleteArea(3);
        assertEquals(1, rowNo);
    }
}