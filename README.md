# pulsar

> 实现一个秒杀场景，分布式session封装，全局错误封装，缓存key值封装，通用结果封装，利用redis和mq应对高并发

- SpringBoot
- Mybatis
- Redis
- RabbitMQ
- JSR303
- Druid
- JMeter
- Jquery